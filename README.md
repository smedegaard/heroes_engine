[![build status](https://img.shields.io/gitlab/pipeline/smedegaard/heroes_engine.svg)](https://gitlab.com/smedegaard/heroes_engine/commits/master)
[![coverage report](https://gitlab.com/smedegaard/heroes_engine/badges/master/coverage.svg)](https://gitlab.com/smedegaard/heroes_engine/commits/master)

# HeroesEngine

> A GenServer that implements the logic and state of the app

## Documentation
Docs can be generated with `mix docs`

## About

The engine consists of a `HeroesSupervisor` that spins up instances of `HeroesEngine`s which are implemented as `GenServer`s.

When the state of a `HeroesEngine` is changed the `HeroesSupervisor` stores
the state in an Erlang Terms Storage. If a `HeroesEngine` should crash it is spun up again
and the state is fetched from the ETS table.

`Registry` [https://hexdocs.pm/elixir/Registry.html] is used to make it easier to send messages to the `HeroesEngine`s.
