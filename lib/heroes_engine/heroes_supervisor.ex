defmodule HeroesEngine.HeroesSupervisor do
  use Supervisor

  @doc """
  Added to make testing possible
  """
  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, name: :test)
  end

  def start_link() do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Supervisor.init([HeroesEngine], strategy: :simple_one_for_one)
  end

  @doc """
  Adds a `HeroesEngine` to the supervision tree
  """
  def start_heroes_list(name) do
    Supervisor.start_child(__MODULE__, [name])
  end

  @doc """
  Removes a `HeroesEngine` from the supervision tree
  """
  def stop_heroes_list(name) do
    :ets.delete(:list_state, name)
    Supervisor.terminate_child(__MODULE__, pid_from_name(name))
  end

  @doc """
  Returns a list of names of all the active `HeroesEngine` GenServers or en empty list if there is none
  """
  @spec get_children_names() :: [String.t()] | []
  def get_children_names() do
    pids =
      Supervisor.which_children(__MODULE__)
      |> Enum.map(fn {_, pid, _type, _module_list} -> pid end)

    Enum.map(pids, fn pid -> Registry.keys(Registry.HeroesEngine, pid) end)
    |> List.flatten()
  end

  @doc """
  Returns a `pid` given a name of a `HeroesEngine` 
  """
  defp pid_from_name(name) do
    name
    |> HeroesEngine.via_tuple()
    |> GenServer.whereis()
  end
end
