defmodule HeroesEngine do
  @moduledoc """
  A GenServer that implements the logic and state of the app
  """

  @timeout 60 * 60 * 1000

  use GenServer, start: {__MODULE__, :start_link, []}, restart: :transient, type: :worker
  alias HeroesEngine.Hero

  @doc """
  Starts a `HeroesEngine` `GenServer` and links it to the  current process
  """
  def start_link(list_name) when is_binary(list_name) do
    GenServer.start_link(__MODULE__, list_name, name: via_tuple(list_name))
  end

  @impl GenServer
  def init(list_name) do
    send(self(), {:set_state, list_name})
    {:ok, fresh_state(list_name)}
  end

  @impl GenServer
  def terminate({:shutdown, :timeout}, state_data) do
    :ets.delete(:list_state, state_data.list_name)
    :ok
  end

  @impl GenServer
  def terminate(_reason, _state), do: :ok

  @impl GenServer
  def handle_info(:timeout, state_data) do
    {:stop, {:shutdown, :timeout}, state_data}
  end

  @impl GenServer
  def handle_info({:set_state, list_name}, _state_data) do
    state_data =
      case :ets.lookup(:list_state, list_name) do
        [] -> fresh_state(list_name)
        [{_key, state}] -> state
      end

    :ets.insert(:list_state, {list_name, state_data})
    {:noreply, state_data, @timeout}
  end

  @impl GenServer
  def handle_call({:add_hero, %Hero{} = hero}, _from, state) do
    heroes = state[:heroes]
    id = String.to_atom(hero.name)

    case Map.has_key?(heroes, id) do
      false ->
        new_state = %{state | heroes: Map.put(state[:heroes], id, hero)}
        reply_success({:ok, {:hero_added, new_state}}, new_state)

      true ->
        {:reply, {:error, :hero_exsists}, state}
    end
  end

  @impl GenServer
  def handle_call({:remove_hero, name}, _from, state) do
    case Map.has_key?(state[:heroes], name) do
      true ->
        new_state = %{state | heroes: Map.delete(state[:heroes], name)}
        reply_success({:ok, {:hero_removed, new_state}}, new_state)

      false ->
        {:reply, {:error, :no_such_hero, name}, state}
    end
  end

  @impl GenServer
  def handle_call({:update_hero, id, %Hero{} = hero}, _from, state) do
    if Map.has_key?(state[:heroes], id) do
      new_id = String.to_atom(hero.name)

      new_heroes =
        Map.delete(state[:heroes], id)
        |> Map.put_new(new_id, hero)

      new_state = %{state | heroes: new_heroes}
      reply_success({:ok, {:hero_updated, new_state}}, new_state)
    else
      {:reply, {:error, :no_such_hero}, state}
    end
  end

  @impl GenServer
  def handle_call(:get_heroes, _from, state) do
    {:reply, {:ok, state[:heroes]}, state}
  end

  @impl GenServer
  def handle_call({:get_hero, id}, _from, state) do
    hero = Map.get(state[:heroes], id)
    {:reply, {:ok, hero}, state}
  end

  @impl GenServer
  def handle_call({:get_list_name}, _from, state) do
    {:reply, state[:list_name], state}
  end

  ## PRIVATE

  @doc """
  Stores the state in ETS table and returns reply tuple
  """
  defp reply_success(reply, state_data) do
    :ets.insert(:list_state, {state_data.list_name, state_data})
    {:reply, reply, state_data, @timeout}
  end

  defp fresh_state(list_name) do
    %{
      list_name: list_name,
      heroes: %{}
    }
  end

  ## UTIL
  @doc """
  Given a list name, returns a tuple that the `Registry` of `HeroesEngine`
  can use to idetify the GenServer holding the heroes list.

  ## Examples

    iex> HeroesEngine.via_tuple(:my_list)
    {:via, Registry, {Registry.HeroesEngine, :my_list}}
  """
  @spec via_tuple(atom) :: tuple
  def via_tuple(name), do: {:via, Registry, {Registry.HeroesEngine, name}}

  ## API
  @doc """
  Given the `pid` of an active heroes list and a `%Hero{}` struct
  it returns `{:ok, {:hero_added, new_state}}`.

  If a hero by the given name already exsists it returns
  `{:error, :herro_exsists}`
  """
  @spec add_hero(pid, Hero.t()) :: map
  def add_hero(pid, %Hero{} = hero) do
    GenServer.call(pid, {:add_hero, hero})
  end

  @doc """
  Given the pid of the active heroes list and the `atom` id of the hero
  it removes the the hero from state and returns a tuple with the updated state.
  `{:ok, {:hero_removed, new_state}}`

  Returns `{:error, {:no_such_hero, hero_name}}` if no hero by the given name exsists.
  """
  @spec remove_hero(pid, atom) :: map
  def remove_hero(pid, name) when is_atom(name) do
    GenServer.call(pid, {:remove_hero, name})
  end

  @doc """
  Given the `pid` of the active heroes list it returns
  `{:ok, {heroes: map_of_heroes}}`

  """
  @spec get_heroes(pid) :: map
  def get_heroes(pid) do
    GenServer.call(pid, :get_heroes)
  end

  @doc """
  Given the `pid` of and active list and the id of the hero it returns
  `{:ok,  %Hero{}}`
  """
  @spec get_hero(pid, atom) :: map
  def get_hero(pid, name) when is_atom(name) do
    GenServer.call(pid, {:get_hero, name})
  end

  @doc """
  Takes the `pid` of the GenServer, the `atom` that identifies the hero to be updated
  and a `%Hero{}` struct with the name and power of the updated hero.

  Returns `{:ok, {:hero_updated, new_state}}` or
  `{:error, :no_such_hero}`
  """
  @spec update_hero(pid, atom, Hero.t()) :: map
  def update_hero(pid, id, %Hero{} = hero) do
    GenServer.call(pid, {:update_hero, id, hero})
  end
end
