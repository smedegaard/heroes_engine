defmodule HeroesEngine.Hero do
  @moduledoc """
  `%Hero{}` defines a heroic entity with a `name` and a `power`
  """

  alias __MODULE__

  @type t :: %{
          name: String.t(),
          power: String.t()
        }

  @enforce_keys [:name, :power]
  defstruct [
    :name,
    :power
  ]

  @spec new(name :: String.t(), power :: String.t()) :: Hero.t()
  @doc """
  Creates a new `%Hero`

  ## Examples

  iex> Heroes.Hero.new("Luke Skywalker", "Being awesome")
  iex> Heroes.Hero{name: "Luke Skywalker", power: "Being awesome"}
  """

  def new(name, power) do
    struct(__MODULE__,
      name: name,
      power: power
    )
  end
end
