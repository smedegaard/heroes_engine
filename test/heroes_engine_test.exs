defmodule HeroesEngineTest do
  use ExUnit.Case, async: true
  doctest HeroesEngine

  alias HeroesEngine.Hero
  alias HeroesEngine.HeroesSupervisor

  @hero_name "Testman"
  @hero_power "testing"

  setup do
    # Random list_name so I avoid testing on an old state
    # Might be a more bullet proof way to randomize?
    list_name =
      Enum.random(1..1_000_000)
      |> Integer.to_string()

    IO.puts("\n" <> list_name)

    pid =
      start_supervised!(HeroesSupervisor,
        start: {HeroesEngine, :start_link, [list_name]},
        restart: :transient,
        type: :worker
      )

    hero = Hero.new(@hero_name, @hero_power)

    {:ok, %{hero: hero, pid: pid, list_name: list_name}}
  end

  describe "Tests for starting a HeroesEngine" do
    test "start_link/1 starts a GenServer" do
      {:ok, pid} = HeroesEngine.start_link("list_name")
      assert is_pid(pid)
    end

    test "init/1 returns {:ok, fresh_state}" do
      value = HeroesEngine.init("list_name")
      assert value == {:ok, %{list_name: "list_name", heroes: %{}}}
    end
  end

  describe "handle_call tests" do
    test "handle_in(pid, {:add_hero, hero})", %{pid: pid, hero: hero, list_name: list_name} do
      assert GenServer.call(pid, {:add_hero, hero}) ==
               {:ok,
                {:hero_added,
                 %{list_name: list_name, heroes: %{String.to_atom(hero.name) => hero}}}}
    end

    test "cannot insert hero with same name", %{pid: pid, hero: hero, list_name: list_name} do
      GenServer.call(pid, {:add_hero, hero})
      assert GenServer.call(pid, {:add_hero, hero}) == {:error, :hero_exsists}
    end

    test "handle_call({:remove_hero, id})", %{pid: pid, hero: hero, list_name: list_name} do
      GenServer.call(pid, {:add_hero, hero})
      result = GenServer.call(pid, {:remove_hero, String.to_atom(hero.name)})
      assert result == {:ok, {:hero_removed, %{list_name: list_name, heroes: %{}}}}
    end

    test "handle_call({:update_hero, %Hero{}})", %{pid: pid, hero: hero, list_name: list_name} do
      HeroesEngine.add_hero(pid, hero)
      id = String.to_atom(hero.name)
      new_hero = Hero.new("NewName", "New power")
      {:ok, {:hero_updated, new_state}} = HeroesEngine.update_hero(pid, id, new_hero)
      assert Map.has_key?(new_state[:heroes], String.to_atom("NewName"))
    end

    test "handle_call(:get_heroes, _from, state)", %{pid: pid, hero: hero, list_name: list_name} do
      HeroesEngine.add_hero(pid, hero)
      result = GenServer.call(pid, :get_heroes)
      assert result == {:ok, %{String.to_atom(hero.name) => hero}}
    end

    test "handle_call({:get_hero, id})", %{pid: pid, hero: hero, list_name: list_name} do
      HeroesEngine.add_hero(pid, hero)
      result = GenServer.call(pid, {:get_hero, String.to_atom(hero.name)})
      assert result == {:ok, hero}
    end
  end

  describe "Tests for the public functions of HeroesEngine" do
    test "add_hero() adds a hero to list", %{hero: hero, pid: pid} do
      {:ok, {:hero_added, new_state}} = HeroesEngine.add_hero(pid, hero)
      assert Map.has_key?(new_state[:heroes], String.to_atom(@hero_name))
    end

    test "remove hero from list", %{hero: hero, pid: pid} do
      {:ok, {:hero_added, _new_state}} = HeroesEngine.add_hero(pid, hero)

      {:ok, {:hero_removed, new_state}} =
        HeroesEngine.remove_hero(pid, String.to_atom(@hero_name))

      refute Map.has_key?(new_state, String.to_atom(@hero_name))
    end

    test "get_heroes() returns a map of heroes", %{hero: hero, pid: pid} do
      ## Add 2 heroes
      {:ok, {:hero_added, _new_state}} = HeroesEngine.add_hero(pid, hero)
      sidekick = Hero.new("Sidekick", "helping out")
      {:ok, {:hero_added, new_state}} = HeroesEngine.add_hero(pid, sidekick)

      assert new_state[:heroes] == %{
               Testman: %Hero{name: @hero_name, power: @hero_power},
               Sidekick: %Hero{name: "Sidekick", power: "helping out"}
             }
    end

    test "get_hero/1 returns the hero with the given id", %{hero: hero, pid: pid} do
      {:ok, {:hero_added, _new_state}} = HeroesEngine.add_hero(pid, hero)
      id = String.to_atom(hero.name)
      {:ok, fetched_hero} = HeroesEngine.get_hero(pid, id)
      assert hero === fetched_hero
    end

    test "update_hero/2 updates the hero with the given id and returns the updated state", %{
      hero: hero,
      pid: pid
    } do
      {:ok, {:hero_added, _new_state}} = HeroesEngine.add_hero(pid, hero)
      id = String.to_atom(hero.name)

      new_hero = Hero.new("NewName", "New power")

      {:ok, {:hero_updated, new_state}} = HeroesEngine.update_hero(pid, id, new_hero)
      assert Map.has_key?(new_state[:heroes], String.to_atom("NewName"))
    end

    test "via_tuple/1 returns a via-tuple" do
      assert {:via, Registry, {Registry.HeroesEngine, :test}} == HeroesEngine.via_tuple(:test)
    end
  end
end
