defmodule HeroesEngineProps do
  use  ExUnit.Case
  use PropCheck
  use PropCheck.StateM

  alias HeroesEngine.Hero
  alias HeroesEngine.HeroesSupervisor

  property "stateful property" do
    forall cmds <- commands(__MODULE__) do
      HeroesEngine.start_link()

      {history, state, result} = run_commands(__MODULE__, cmds)
      HeroesEngine.stop()

      (result == :ok)
      |> aggregate(command_names(cmds))
      |> when_fail(
        IO.puts("""
        History: #{inspect(history)}
        State: #{inspect(state)}
        Result: #{inspect(result)}
        """)
      )
    end
  end

  def initial_state() do
    %{
      list_name: "name of list",
      heroes: %{}
    }
  end

  def command(_state) do
    oneof([
{:call, HeroesEngine, :add_hero, []}
    ])
  end
end
